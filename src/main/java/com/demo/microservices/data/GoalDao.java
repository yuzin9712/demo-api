package com.demo.microservices.data;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.demo.microservices.model.Goal;
import com.demo.microservices.model.UserGoal;

@Mapper
public interface GoalDao {
	/**
	 * 목표 정보 가져오기 
	 * @return
	 * @throws Exception
	 */
	List<Goal> selectGoal() throws Exception;	
	
	/**
	 * 아이디로 목표 정보 확인하기
	 * @param goalId
	 * @return
	 * @throws Exception
	 */
	Goal selectGoalById(String goalId) throws Exception;	
	
	/**
	 * 목표 정보 변경하
	 * @param sampleGoal
	 * @return
	 * @throws Exception
	 */
	int updateGoal(Goal sampleGoal) throws Exception;
	
	/**
	 * 목표 등록하기 
	 * @param sampleGoal
	 * @return
	 * @throws Exception
	 */
	int insertGoal(Goal sampleGoal) throws Exception;
	
	/**
	 * 목표 정보 삭제하기 
	 * @param goalId
	 * @return
	 * @throws Exception
	 */
	int deleteUser(String goalId) throws Exception;		
	
	/**
	 * 테스트 목표 등록하기 
	 * @param ArrayList<Goal> goals
	 * @return
	 * @throws Exception
	 */
	int createTestGoals(ArrayList<Goal> goals) throws Exception;

	/**
	 * 진행중인 목표 등록하기 
	 * @param UserGoal userGoal
	 * @return
	 * @throws Exception
	 */
	int insertUserGoal(UserGoal userGoal) throws Exception;

	/**
	 * 진행중인 목표 수정하기 
	 * @param UserGoal userGoal
	 * @return
	 * @throws Exception
	 */
	int updateUserGoal(UserGoal userGoal) throws Exception;

	/**
	 * 회원 아이디로 진행중인 목표 조회하기 
	 * @param String userId
	 * @return
	 * @throws Exception
	 */
	List<Goal> selectUserGoalById(String userId) throws Exception;

	/**
	 * 진행중인 목표 상세 조회하기 
	 * @param String userGoalId
	 * @return
	 * @throws Exception
	 */
	UserGoal selectUserGoal(String userGoalId) throws Exception;

}
