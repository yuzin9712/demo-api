package com.demo.microservices.domain;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.demo.microservices.data.GoalDao;
import com.demo.microservices.model.Goal;
import com.demo.microservices.model.UserGoal;

@Service
public class GoalDomain {
	private final Logger log = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private GoalDao sampleGoalDao;
	
	/*
	 * getGoalList: 목표 목록을 100개까지 리턴
	 */
	public ResponseEntity<List<Goal>> getGoalList() { 
		
		List<Goal> list = null;
		try {
			log.info("Start db select");
			list = sampleGoalDao.selectGoal();
		} catch (Exception e) {
			e.printStackTrace();
		}
		log.debug("user counts :"+list.size());
		
		return new ResponseEntity<List<Goal>> (list, HttpStatus.OK);
	}
	
	/*
	 * getGoalById: goalId에 해당하는 목표정보 리턴 
	 */
	public ResponseEntity <Goal> getGoalById(String goalId) { 
		Goal re = null;
		try {
			log.info("Start db select");
			re = sampleGoalDao.selectGoalById(goalId);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<Goal> (re, HttpStatus.OK);
	}
	
	/*
	 * setGoalUpdate: 목표 정보 변경 
	 */
	public ResponseEntity <String > setGoalUpdate(String userId, Goal sampleGoal) throws Exception { 
		log.info("Start db update==>"+userId);

		int re  = sampleGoalDao.updateGoal(sampleGoal);
		log.debug("result :"+ re);
		
		return new ResponseEntity<String> (re+"", HttpStatus.OK);
	}

	/*
	 * setGoalInsert: 목표 추가 
	 */
	public ResponseEntity <String > setGoalInsert(Goal sampleGoal) throws Exception { 
		log.info("Start db insert");
		int re  = sampleGoalDao.insertGoal(sampleGoal);
		log.debug("result :"+ re);
		
		return new ResponseEntity<String> (re+"", HttpStatus.OK);
	}
	
	/*
	 *  setGoalDelete: 목표 삭제 
	 */
	public ResponseEntity <String > setGoalDelete(String goalId) throws Exception { 
		log.info("Start db insert");
		int re  = sampleGoalDao.deleteUser(goalId);
		log.debug("result :"+ re);
		
		return new ResponseEntity<String> (re+"", HttpStatus.OK);
	}

	public ResponseEntity<String> setUserGoalInsert(UserGoal userGoal) throws Exception {
		log.info("Start db insert");
		int re  = sampleGoalDao.insertUserGoal(userGoal);
		log.debug("result :"+ re);
		
		return new ResponseEntity<String> (re+"", HttpStatus.OK);
	}

	public ResponseEntity<String> setUserGoalUpdate(String userGoalId, UserGoal userGoal) throws Exception {
		log.info("Start db update==>"+userGoalId);

		int re  = sampleGoalDao.updateUserGoal(userGoal);
		log.debug("result :"+ re);
		
		return new ResponseEntity<String> (re+"", HttpStatus.OK);
	}

	public ResponseEntity<List<Goal>> getUserGoalById(String userId) {
		List<Goal> re = null;
		try {
			log.info("Start db select");
			re = sampleGoalDao.selectUserGoalById(userId);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<List<Goal>> (re, HttpStatus.OK);
	}

	public ResponseEntity<UserGoal> getUserGoal(String userGoalId) throws Exception {
		UserGoal re = null;
		try {
			log.info("Start db select");
			re = sampleGoalDao.selectUserGoal(userGoalId);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<UserGoal> (re, HttpStatus.OK);
	}
}
