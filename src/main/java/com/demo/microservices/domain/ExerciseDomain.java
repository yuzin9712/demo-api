package com.demo.microservices.domain;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.microservices.data.ExerciseDao;
import com.demo.microservices.model.CurrentExercise;
import com.demo.microservices.model.ExerciseRecord;
import com.demo.microservices.model.GoalStatus;
import com.demo.microservices.model.UserExerciseRank;

import lombok.RequiredArgsConstructor;

@Service
public class ExerciseDomain {
	

	public CurrentExercise getCurrentExercise() {
		return null;
	}

	public List<ExerciseRecord> getCurrentExerciseRecord() {
		return null;
	}

	public List<UserExerciseRank> getUserExerciseRank() {
		return null;
	}

	public GoalStatus getGoalStatus() {
		return null;
	}

}
