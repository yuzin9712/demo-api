package com.demo.microservices.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.demo.microservices.domain.GoalDomain;
//import com.demo.microservices.domain.TestDomain;
import com.demo.microservices.model.Goal;
import com.demo.microservices.model.UserGoal;

@Service
public class GoalService {
	@Autowired
	private GoalDomain goalDomain;
//	
//	@Autowired
//	private TestDomain testDomain;
	
	public ResponseEntity <List<Goal>> getGoalList() { 
		return goalDomain.getGoalList();
	}
	
	public ResponseEntity <Goal> getGoalById(String goalId) { 
		return goalDomain.getGoalById(goalId);
	}
	
	public ResponseEntity <String > setGoalUpdate(String goalId, Goal sampleGoal) throws Exception {
		return goalDomain.setGoalUpdate(goalId, sampleGoal);
	}
	
	public ResponseEntity <String > setGoalInsert(Goal sampleGoal) throws Exception { 
		return goalDomain.setGoalInsert(sampleGoal);		
	}

	public ResponseEntity <String > setGoalDelete(String goalId) throws Exception { 
		return goalDomain.setGoalDelete(goalId);
	}

	public ResponseEntity<String> setUserGoalInsert(UserGoal userGoal) throws Exception {
		return goalDomain.setUserGoalInsert(userGoal);
	}

	public ResponseEntity<String> setUserGoalUpdate(String userGoalId, UserGoal userGoal) throws Exception {
		return goalDomain.setUserGoalUpdate(userGoalId, userGoal);
	}

	public ResponseEntity<List<Goal>> getUserGoalById(String userId) throws Exception {
		return goalDomain.getUserGoalById(userId);
	}

	public ResponseEntity<UserGoal> getUserGoal(String userGoalId) throws Exception {
		return goalDomain.getUserGoal(userGoalId);
	}
	
//	public ResponseEntity <String > createTestGoals(int startGoalId, int goalCount) throws Exception { 
//		return goalDomain.createTestGoals(startGoalId, goalCount);
//	}
}
