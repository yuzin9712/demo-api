package com.demo.microservices.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.demo.microservices.domain.ExerciseDomain;
import com.demo.microservices.model.CurrentExercise;
import com.demo.microservices.model.ExerciseRecord;
import com.demo.microservices.model.GoalStatus;
import com.demo.microservices.model.UserExerciseRank;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class ExerciseService {
	
	private final ExerciseDomain exerciseDomain;
	
	public CurrentExercise getCurrentExercise() {
		return exerciseDomain.getCurrentExercise();
	}

	public List<ExerciseRecord> getCurrentExerciseRecord() {
		return exerciseDomain.getCurrentExerciseRecord();
	}

	public List<UserExerciseRank> getUserExerciseRank() {
		return exerciseDomain.getUserExerciseRank();
	}

	public GoalStatus getGoalStatus() {
		return exerciseDomain.getGoalStatus();
	}

}
