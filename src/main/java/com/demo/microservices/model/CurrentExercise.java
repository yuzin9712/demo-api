package com.demo.microservices.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter @Setter
public class CurrentExercise {
	
	private Long step;
	private Long dailyGoal;
	private Long dailySuccessRate;
	

}
