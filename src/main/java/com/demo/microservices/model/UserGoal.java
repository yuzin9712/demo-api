package com.demo.microservices.model;

public class UserGoal {
	private String 	userGoalId  ; // 회원당 목표 아이디
	private String  goalId 		; // 목표 아이디
	private String  userId      ; // 회원 아이디
	private int     isDoing     ; // 진행중인 상태 체크 (0-진행중, 1- 완료, 2- 포기)
	private int     currentDo   ; // 현재 진행한 걸음 수, 웨이트 갯수, 따릉이 km 수 표기
	public UserGoal() {
		
	}
	public String getGoalId() {
		return goalId;
	}
	public void setGoalId(String goalId) {
		this.goalId = goalId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public int getIsDoing() {
		return isDoing;
	}
	public void setIsDoing(int isDoing) {
		this.isDoing = isDoing;
	}
	public int getCurrentDo() {
		return currentDo;
	}
	public void setCurrentDo(int currentDo) {
		this.currentDo = currentDo;
	}

}
