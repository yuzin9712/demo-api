package com.demo.microservices.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter	@Setter
public class UserExerciseRank {
	
	private String id;	//회원아이디
	private String starId;	//스타뱅킹 아이디
	private String name;
	private Long step;

}
