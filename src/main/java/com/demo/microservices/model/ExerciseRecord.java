package com.demo.microservices.model;

import java.time.LocalDateTime;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter	@Setter
public class ExerciseRecord {
	
	private Long step;
	private LocalDateTime exerciseDate;
	private Boolean isSucceed;
	
}
