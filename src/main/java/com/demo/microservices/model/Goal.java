package com.demo.microservices.model;

public class Goal {
	private String  goalId 		; // 목표 아이디
	private String  goalNm 		; // 목표 이름 
	private String  goalPoint 	  ; // 달성시 획득 포인트 
	public Goal() {
		
	}
	
	public String getGoalId() {
		return goalId;
	}
	public void setGoalId(String goalId) {
		this.goalId = goalId;
	}
	public String getUserNm() {
		return goalNm;
	}
	public void setGoalNm(String goalNm) {
		this.goalNm = goalNm;
	}
	
	public String getGoalPoint() {
		return goalPoint;
	}
	public void setGoalPoint(String goalPoint) {
		this.goalPoint = goalPoint;
	}
}
