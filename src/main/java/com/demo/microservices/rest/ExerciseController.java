package com.demo.microservices.rest;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.microservices.model.CurrentExercise;
import com.demo.microservices.model.ExerciseRecord;
import com.demo.microservices.model.GoalStatus;
import com.demo.microservices.model.UserExerciseRank;
import com.demo.microservices.service.ExerciseService;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RequestMapping("/exercise")
@RestController
public class ExerciseController {

	/*
	 * 1. 운동 데이터 받아오기 - external system 사용할 경우
	 * 
	 * 2. 운동 현황 조회(달성률, 전체 목표 걸음 수, 누적된 걸음 수)
	 * 
	 * 3. 운동 기록 조회(어떤 목표의 운동기록을 가져 올 것인지)
	 * 
	 * 4. 운동 랭킹 조회
	 * 
	 */
	
	private final ExerciseService exerciseService;
	
	/*
	 * External System 사용시, 토큰을 헤더에 담아야 한다.
	 */
	@ApiOperation(value="현재까지의 걸음수를 구글 피트니스와 연동해서 가져온다.")
	@GetMapping("")
	public ResponseEntity<CurrentExercise> getCurrentExercise() {
		
		CurrentExercise currentExercise = exerciseService.getCurrentExercise();
		return new ResponseEntity<>(currentExercise, HttpStatus.OK);
	}
	
	@ApiOperation(value="선택한 목표의 운동기록을 날짜순으로 보여준다.")
	@GetMapping("/record/{goalId}")
	public ResponseEntity<List<ExerciseRecord>> getExerciseRecord(@PathVariable Long goalId) {
		
		List<ExerciseRecord> exerciseRecordList = exerciseService.getCurrentExerciseRecord();
		return new ResponseEntity<>(exerciseRecordList, HttpStatus.OK);
	}
	
	@ApiOperation(value="어제의 운동 기록 랭킹을 조회한다.")
	@GetMapping("/rank")
	public ResponseEntity<List<UserExerciseRank>> getUserExerciseRank() {
		List<UserExerciseRank> userExerciseRank = exerciseService.getUserExerciseRank();
		return new ResponseEntity<>(userExerciseRank, HttpStatus.OK);
		
	}
	
	@ApiOperation(value="목표의 현황을 조회한다.")
	@GetMapping("/goal/{goalId}")
	public ResponseEntity<GoalStatus> getGoalStatus() {
		GoalStatus goalStatus = exerciseService.getGoalStatus();
		return new ResponseEntity<>(goalStatus, HttpStatus.OK);
	}
	
	
}
