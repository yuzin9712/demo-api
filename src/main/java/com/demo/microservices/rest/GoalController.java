package com.demo.microservices.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.demo.microservices.model.Goal;
import com.demo.microservices.model.UserGoal;
import com.demo.microservices.service.GoalService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value="Goal API")
@RestController
public class GoalController {
	@Autowired
	private GoalService goalService;
	
	@GetMapping("/goals")	
	@ApiOperation(value="목표 정보 가져오기", notes="목표 정보를 제공합니다. ")
	public ResponseEntity<List<Goal>> getGoalList() { 
		return goalService.getGoalList();
	}
	
	
	@GetMapping("/goals/{goalId}")
	@ApiOperation(value="아이디로 목표 상세 정보 가져오기 ")
	public ResponseEntity <Goal> getGoalById(
				@PathVariable (name="goalId", required = true) String goalId
			) { 
		return goalService.getGoalById(goalId);
	}
	
	@PutMapping("/goals/{goalId}")
	@ApiOperation(value="목표 정보 변경하기 ")
	public ResponseEntity <String > setGoalUpdate(
			@PathVariable(name="goalId",required = true ) String goalId, 
			@RequestBody Goal sampleGoal
		) throws Exception { 
		
		return goalService.setGoalUpdate(goalId, sampleGoal);
	}
	
	@PostMapping("/goals")
	@ApiOperation(value="목표 정보 등록하기 ")
	public ResponseEntity <String > setGoalInsert(
			@RequestBody Goal sampleGoal
		) throws Exception { 
		
		return goalService.setGoalInsert(sampleGoal);
	}
	
	@DeleteMapping("/goals/{goalId}")
	@ApiOperation(value="목표 정보 삭제하기 ")
	public ResponseEntity <String > setGoalDelete(
			@PathVariable(name="goalId",required = true ) String goalId
		) throws Exception { 
		
		return goalService.setGoalDelete(goalId);
	}
	
	@GetMapping("/usergoalsbyid/{userId}")
	@ApiOperation(value="사용자 진행 목표 조회하기")
	public ResponseEntity <List<Goal>> getUserGoalById(
				@PathVariable(name="userId", required = true) String userId
			) throws Exception {
		return goalService.getUserGoalById(userId);
	}
	
	@GetMapping("/usergoals/{userGoalId}")
	@ApiOperation(value = "진행 목표 상세 조회")
	public ResponseEntity <UserGoal> getUserGoal(
			@PathVariable(name="userGoalId", required = true) String userGoalId
		) throws Exception {
	return goalService.getUserGoal(userGoalId);
	}
	
	@PostMapping("/usergoals")
	@ApiOperation(value="사용자 진행 목표 추가하기")
	public ResponseEntity<String> setUserGoal(
			@RequestBody UserGoal userGoal
			) throws Exception {
			return goalService.setUserGoalInsert(userGoal);
	}
	
	@PutMapping("/usergoals")
	@ApiOperation(value="사용자 진행 목표 수정하기")
	public ResponseEntity<String> setUserGoalUpdate(
			@PathVariable(name="userGoalId", required = true) String userGoalId,
			@RequestBody UserGoal userGoal
			) throws Exception{
		return goalService.setUserGoalUpdate(userGoalId, userGoal);
	}
//	@GetMapping("/createtestusers/{startUserId}/{userCount}")
//	@ApiOperation(value="테스트 사용자를 userCount명 등록하기 ")
//	public ResponseEntity <String > createTestUsers(
//			@PathVariable (name="startUserId", required = true) int startUserId,
//			@PathVariable (name="userCount", required = true) int userCount
//		) throws Exception { 
//		
//		return userService.createTestUsers(startUserId, userCount);
//	}	
}
